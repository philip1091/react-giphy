import React, { Component } from "react";
import Gif from './gif.jsx';

const GifList = ({gifs, selected}) => {
  return (
    <div className="gif-list">
      {gifs.map(({id}) => <Gif id={id} key={id} selected={selected} />)}
    </div>
  );
};
export default GifList;
