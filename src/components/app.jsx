import React, {Component} from 'react';
import giphy from 'giphy-api';

import SearchBar from './search_bar.jsx';
import Gif from './gif.jsx';
import GifList from './gif_list.jsx';

class App extends Component {

  constructor(props){
    super(props)

    this.state = {
      gifs: [],
      selectedGifId: null//"UtyJkFjwPP5NS"
    }
  }

  selected = (id) => {
    this.setState({
      selectedGifId: id
    });
  }

  search = (query) => {
    giphy('Ezqyc6nMQslnC3gCPha1yFVhxbJheR5j').search({
    q: query,
    limit:10,
    rating: 'g'
    }, (err, res) => {
        this.setState({
          gifs:res.data
        })
    });
  }

  render(){

    return (
      <div>
        <div className="left-scene">
          <SearchBar searchFunction = {this.search} />
          <div className="selected-gif">
            <Gif id = {this.state.selectedGifId}/>
          </div>
        </div>
        <div className="right-scene">
          <GifList gifs={this.state.gifs} selected = {this.selected}/>
        </div>
      </div>
    )
  }
}

export default App;
