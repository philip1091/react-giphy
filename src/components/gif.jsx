import React, { Component } from "react";

class Gif extends Component{

  handleClick = () =>{
    if(this.props.selected){
      this.props.selected(this.props.id);
    }else{
      console.log("no")
    }
  }

shouldComponentUpdate(nextProps, nextState) {
  return nextProps !== this.props;
}


  render(){
    console.log("gif render", this.props.id)
    if(!this.props.id){
      return null;
    }
    const src = `https://media.giphy.com/media/${this.props.id}/giphy.gif`;
    return(
      <img onClick={this.handleClick} className="gif" src={src} alt="gif"/>
    )
  }
}
export default Gif;
