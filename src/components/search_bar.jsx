import React, { Component } from "react";

class SearchBar extends Component{

  constructor(props) {
    super(props);

    this.state = {
      term : ""
    }
  }


  handleUpdate = (event) => {
    this.props.searchFunction(event.target.value)
    this.setState({
      term: event.target.value
    })
  }

  componentWillMount(){
    // console.log("search_bar will mount");
  }

  componentDidMount() {
    // console.log("search_bar did mount")
  }


  render(){
        // console.log("search_bar render")

    return(
      <input value = {this.state.term} type="text" className="form-search  form-control" onChange={this.handleUpdate}/>
    )
  }
}
export default SearchBar;
