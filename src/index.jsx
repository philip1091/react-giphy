// external modules
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

// internal modules
import App from './components/app';
import '../assets/stylesheets/application.scss';



const root = document.getElementById('root');
if(root){
  ReactDOM.render(<App />,root)
}

